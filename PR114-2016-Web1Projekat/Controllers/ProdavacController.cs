﻿using PR114_2016_Web1Projekat.Baza;
using PR114_2016_Web1Projekat.Enums;
using PR114_2016_Web1Projekat.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PR114_2016_Web1Projekat.Controllers
{
    public class ProdavacController : Controller
    {
        OperacijePodataka podaciZaSesiju;
        Dictionary<int, ManifestacijaModel> mani = new Dictionary<int, ManifestacijaModel>();
        Dictionary<int, ManifestacijaModel> maniOdobri = new Dictionary<int, ManifestacijaModel>();
        Dictionary<int, ManifestacijaModel> maniOdbij = new Dictionary<int, ManifestacijaModel>();
        Dictionary<int, KartaModel> rezKarte = new Dictionary<int, KartaModel>();
        Dictionary<int, KomentarModel> komentari = new Dictionary<int, KomentarModel>();
        public int param;
       
        // GET: Prodavac
        public ActionResult Index()
        {
            SesijaStart();
            return View("DodajManifestaciju");
        }
        public ActionResult Index1(int id)
        {
            SesijaStart();
            param = id;
            ViewBag.id = param;
            ViewBag.manifestacijaModel = podaciZaSesiju.Manifestacije[id];
           
            return View("IzmeniManifestaciju");
        }
        public ActionResult KomentariZaPrihvacanje()
        {
            SesijaStart();

            
            foreach(var item in podaciZaSesiju.Manifestacije.Values)
            {
                if(item.ProdavacKojiJeKreirao.KorisnickoIme==podaciZaSesiju.UlogovaniKorisnik.KorisnickoIme && item.DaLiJeObrisana==false)
                {
                    mani.Add(item.Id, item);
                   
                }
            }
            ViewBag.komentari = podaciZaSesiju.Komentari;
            ViewBag.ulogovan = podaciZaSesiju.UlogovaniKorisnik;
            ViewBag.manifestacije = mani;
            return View("KomentariZaPrihvacanje");
        }
        public ActionResult RezervisaneKarte()
        {
            SesijaStart();

            foreach (var item in podaciZaSesiju.Karte.Values)
            {
                if (item.DaLiJeObrisana == false && item.StatusKarte == StatusKarte.REZERVISANA)
                {
                    foreach (var jtem in podaciZaSesiju.Manifestacije.Values)
                    {

                        if (item.Manifestacija.Id == jtem.Id)
                        {
                            
                                
                                if (jtem.ProdavacKojiJeKreirao.KorisnickoIme == podaciZaSesiju.UlogovaniKorisnik.KorisnickoIme)
                                {
                                    rezKarte.Add(item.Id, item);
                                }
                            
                        }

                    }
                }
            }
           
            ViewBag.spisak = rezKarte;
            return View("RezKarte");
        }
        public ActionResult SveKom()
        {
            SesijaStart();
            foreach(var  item in podaciZaSesiju.Komentari.Values)
            {
                if(item.DaLiJeIzbrisan==false)
                {
                    komentari.Add(item.Id, item);
                }
            }
            ViewBag.komentari = komentari;
            return View("SveKom");
        }
        [HttpPost]
        public ActionResult DodajManifestaciju(ManifestacijaModel formManifestacija)
        {
            SesijaStart();

            if (formManifestacija.Naziv == null || formManifestacija.BrojMesta==0 || formManifestacija.DatumiVremeOdrzavanja==null 
                || formManifestacija.DatumiVremeZavrsetka==null || formManifestacija.CenaREGULARKarte==0 || formManifestacija.MestoOdrzavanja.UlicaiBrojGradPostanskiBroj==null)
            {
                ViewBag.greska = "Niste uneli sve podatke o manifestaciji. Pokusajte ponovo!";
                return View("GreskaDodavanjaManifestacije");
            }
            foreach (var item in podaciZaSesiju.Manifestacije.Values)
            {
                if (item.DatumiVremeOdrzavanja.ToString("hh:mm tt") == formManifestacija.DatumiVremeOdrzavanja.ToString("hh:mm tt") 
                    && item.MestoOdrzavanja.UlicaiBrojGradPostanskiBroj==formManifestacija.MestoOdrzavanja.UlicaiBrojGradPostanskiBroj
                    && item.DaLiJeObrisana==false
                    && item.DatumiVremeOdrzavanja.Date==formManifestacija.DatumiVremeOdrzavanja.Date)
                {
                    ViewBag.greska = "Neka od manifestacija se vec odrzava na ovom mestu u ovo vreme. Pokusajte da zakazete neki drugi termin!";
                    return View("GreskaDodavanjaManifestacije");
                }
            }
            ManifestacijaModel manifestacija = new ManifestacijaModel();
            manifestacija.Id = podaciZaSesiju.Manifestacije.Count + 1;//svaki nov dodat ima za jedan veci id
            manifestacija.Naziv = formManifestacija.Naziv;
            manifestacija.TipManifestacije = formManifestacija.TipManifestacije;
            manifestacija.BrojMesta = formManifestacija.BrojMesta;
            manifestacija.DatumiVremeOdrzavanja = formManifestacija.DatumiVremeOdrzavanja;
            manifestacija.DatumiVremeZavrsetka = formManifestacija.DatumiVremeZavrsetka;
            manifestacija.CenaREGULARKarte = formManifestacija.CenaREGULARKarte;
            manifestacija.Status = StatusManifestacije.NEAKTIVNO;
            manifestacija.MestoOdrzavanja = formManifestacija.MestoOdrzavanja;
            manifestacija.PosterURL = formManifestacija.PosterURL;
            manifestacija.ProdavacKojiJeKreirao.KorisnickoIme = podaciZaSesiju.UlogovaniKorisnik.KorisnickoIme;
            manifestacija.DaLiJeObrisana = false;
            for(int i=0; i<manifestacija.BrojMesta;i++)
            {
                KartaModel karta = new KartaModel();
                karta.StatusKarte = StatusKarte.NEREZERVISANA;
                karta.Id = podaciZaSesiju.Karte.Count + 1;
                karta.Manifestacija = manifestacija;
                karta.DaLiJeObrisana = false;

                if(i%3==0)
                {
                    karta.TipKarte = TipKarte.REGULAR;
                    karta.Cena = manifestacija.CenaREGULARKarte;
                }
                else if(i%3==1)
                {
                    karta.TipKarte = TipKarte.VIP;
                    karta.Cena = manifestacija.CenaREGULARKarte*4;

                }
                else if(i%3==2)
                {
                    karta.TipKarte = TipKarte.FANPIT;
                    karta.Cena = manifestacija.CenaREGULARKarte * 2;
                }
                podaciZaSesiju.Karte.Add(karta.Id, karta);
                podaciZaSesiju.UpisiKarteUFajl(karta);
            }

            //popunjavanje podataka svih
            podaciZaSesiju.Manifestacije.Add(manifestacija.Id, manifestacija);
            podaciZaSesiju.UpisiManifestacijeUFajl(manifestacija);
            ViewBag.registrovan = podaciZaSesiju.UlogovaniKorisnik;//ovaj korisnik je trenutno ulogovan
            return View("../LogovanKorisnik/LogovanKorisnikProdavac");

        }
        public ActionResult SveManifestacijeProdavca()
        {
            SesijaStart();
            Dictionary<int, ManifestacijaModel> manifestacijeKupaca = new Dictionary<int, ManifestacijaModel>();
            foreach(var item in podaciZaSesiju.Manifestacije.Values)
            {
                
                if(item.ProdavacKojiJeKreirao.KorisnickoIme==podaciZaSesiju.UlogovaniKorisnik.KorisnickoIme)
                {
                    manifestacijeKupaca.Add(item.Id, item);
                }

            }

            ViewBag.mojeManifestacije = manifestacijeKupaca;
            return View("ManifestacijeProdavca");


        }
        [HttpPost]
        public ActionResult IzmeniManifestaciju(ManifestacijaModel formIzmenaManifestacija)
        {
            SesijaStart();

            if (formIzmenaManifestacija.Naziv == null  || formIzmenaManifestacija.DatumiVremeOdrzavanja == null
                || formIzmenaManifestacija.DatumiVremeZavrsetka == null || formIzmenaManifestacija.CenaREGULARKarte == 0 || formIzmenaManifestacija.MestoOdrzavanja.UlicaiBrojGradPostanskiBroj == null)
            {
                ViewBag.greska = "Niste uneli sve podatke o manifestaciji. Pokusajte ponovo!";
                return View("GreskaIzmeneManifestacije");
            }
            foreach (var item in podaciZaSesiju.Manifestacije.Values)
            {
                if (item.DatumiVremeOdrzavanja.ToString("hh:mm tt") == formIzmenaManifestacija.DatumiVremeOdrzavanja.ToString("hh:mm tt")
                    && item.MestoOdrzavanja.UlicaiBrojGradPostanskiBroj == formIzmenaManifestacija.MestoOdrzavanja.UlicaiBrojGradPostanskiBroj
                    && item.DaLiJeObrisana==false
                    && item.DatumiVremeOdrzavanja.Date == formIzmenaManifestacija.DatumiVremeOdrzavanja.Date)
                {
                    if (item.Id != formIzmenaManifestacija.Id)
                    {
                        ViewBag.greska = "Neka od manifestacija se vec odrzava na ovom mestu u ovo vreme. Pokusajte da zakazete neki drugi termin!";
                        return View("GreskaIzmeneManifestacije");
                    }
                }
            }
            ManifestacijaModel novaManifestacija = new ManifestacijaModel();
            novaManifestacija.Id = formIzmenaManifestacija.Id;
            novaManifestacija.Naziv = formIzmenaManifestacija.Naziv;
            novaManifestacija.TipManifestacije = formIzmenaManifestacija.TipManifestacije;
            novaManifestacija.BrojMesta = podaciZaSesiju.Manifestacije[formIzmenaManifestacija.Id].BrojMesta;
            novaManifestacija.DatumiVremeOdrzavanja = formIzmenaManifestacija.DatumiVremeOdrzavanja;
            novaManifestacija.DatumiVremeZavrsetka = formIzmenaManifestacija.DatumiVremeZavrsetka;
            novaManifestacija.CenaREGULARKarte = formIzmenaManifestacija.CenaREGULARKarte;
            novaManifestacija.Status = podaciZaSesiju.Manifestacije[formIzmenaManifestacija.Id].Status;
            novaManifestacija.MestoOdrzavanja = formIzmenaManifestacija.MestoOdrzavanja;
            novaManifestacija.PosterURL = formIzmenaManifestacija.PosterURL;
            novaManifestacija.ProdavacKojiJeKreirao = podaciZaSesiju.UlogovaniKorisnik;

            foreach(var item in podaciZaSesiju.Karte.Values)
            {
                if(item.Manifestacija.Id==novaManifestacija.Id)
                {
                    if (item.TipKarte == TipKarte.REGULAR)
                    {
                        podaciZaSesiju.Karte[item.Id].Cena = novaManifestacija.CenaREGULARKarte;
                    }
                    else if(item.TipKarte==TipKarte.VIP)
                    {
                        podaciZaSesiju.Karte[item.Id].Cena = novaManifestacija.CenaREGULARKarte * 4;
                    }
                    else if(item.TipKarte==TipKarte.FANPIT)
                    {
                        podaciZaSesiju.Karte[item.Id].Cena = novaManifestacija.CenaREGULARKarte * 2;
                    }
                    item.Manifestacija = novaManifestacija;
                }
            }
            podaciZaSesiju.IzmeniKarte();
            podaciZaSesiju.Manifestacije[formIzmenaManifestacija.Id] = novaManifestacija;
            podaciZaSesiju.IzmeniManifestacije();

            ViewBag.mojeManifestacije = podaciZaSesiju.Manifestacije;
            return View("ManifestacijeProdavca");


        }
        public ActionResult OdobriKomentar(int id)
        {
            SesijaStart();
            foreach (var item in podaciZaSesiju.Komentari.Values)
            {
                if (item.Id == id)
                {
                    if (podaciZaSesiju.Komentari[item.Id].DaLiJePrihvacen == false)
                    {
                        podaciZaSesiju.Komentari[item.Id].DaLiJePrihvacen = true;
                        podaciZaSesiju.IzmeniKomentare();
                    }
                    else
                    {
                        ViewBag.greska = "Ovaj komentar je vec prihvacen!";
                        return View("GreskaPrihvacanja");
                    }
                }
            }
            foreach (var item in podaciZaSesiju.Manifestacije.Values)
            {
                if (item.ProdavacKojiJeKreirao.KorisnickoIme == podaciZaSesiju.UlogovaniKorisnik.KorisnickoIme && item.DaLiJeObrisana == false)
                {
                    maniOdobri.Add(item.Id, item);
                   
                }
            }
            ViewBag.komentari = podaciZaSesiju.Komentari;
            ViewBag.manifestacije = maniOdobri;
            return View("KomentariZaPrihvacanje");
        }
        public ActionResult OdbijKomentar(int id)
        {
            SesijaStart();
            foreach (var item in podaciZaSesiju.Komentari.Values)
            {
                if (item.Id == id)
                {
                    if (podaciZaSesiju.Komentari[item.Id].DaLiJePrihvacen == false)
                    {
                        podaciZaSesiju.Komentari[item.Id].DaLiJePrihvacen = false;
                        podaciZaSesiju.IzmeniKomentare();
                    }
                    else
                    {
                        ViewBag.greska = "Ovaj komentar je vec prihvacen!";
                        return View("GreskaPrihvacanja");
                    }
                }
            }
            foreach (var item in podaciZaSesiju.Manifestacije.Values)
            {
                if (item.ProdavacKojiJeKreirao.KorisnickoIme == podaciZaSesiju.UlogovaniKorisnik.KorisnickoIme && item.DaLiJeObrisana == false)
                {
                    maniOdbij.Add(item.Id, item);

                }
            }
            ViewBag.komentari = podaciZaSesiju.Komentari;
            ViewBag.mani = maniOdbij;
            return View("KomentariZaPrihvacanje");
        }
        private void SesijaStart()
        {
            podaciZaSesiju = (OperacijePodataka)Session["PodaciZaSesiju"];
            if (podaciZaSesiju == null)
            {
                podaciZaSesiju = new OperacijePodataka();
                Session["PodaciZaSesiju"] = podaciZaSesiju;
            }

            podaciZaSesiju.UcitajPodatkeIznova();

        }
    }
}