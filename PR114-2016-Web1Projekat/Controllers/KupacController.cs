﻿using PR114_2016_Web1Projekat.Baza;
using PR114_2016_Web1Projekat.Enums;
using PR114_2016_Web1Projekat.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PR114_2016_Web1Projekat.Controllers
{
    public class KupacController : Controller
    {
        OperacijePodataka podaciZaSesiju;
        Dictionary<int,KartaModel> karteN=new Dictionary<int, KartaModel>();
        Dictionary<int, KartaModel> karteS = new Dictionary<int, KartaModel>();
        Dictionary<int, KartaModel> karteZ = new Dictionary<int, KartaModel>();
        Dictionary<int, ManifestacijaModel> proslaRezervacija = new Dictionary<int, ManifestacijaModel>();
        Dictionary<int, KomentarModel> odobreniKomentari = new Dictionary<int, KomentarModel>();
        // GET: Kupac
        public ActionResult Index()
        {
            SesijaStart();
            ViewBag.manifestacija = podaciZaSesiju.Manifestacije;
            ViewBag.status = StatusManifestacije.AKTIVNO;
            return View("DostupneManifestacije");
        }
        public ActionResult IndexProfil(int id)
        {
            SesijaStart();
            foreach(var item in podaciZaSesiju.Manifestacije.Values)
            {
               if(item.Id==id)
                {
                    ViewBag.profil = item;
                }
            }
            return View("ProfilManifestacije");
        }
        public ActionResult IndexSNeRez(int id)
        {
            SesijaStart();
           foreach(var item in podaciZaSesiju.Karte.Values)
            {
                if(item.Manifestacija.Id==id && item.DaLiJeObrisana==false && item.StatusKarte==StatusKarte.NEREZERVISANA)
                {
                     karteS.Add(item.Id, item);
                   
                }
            }
            ViewBag.spisak = karteS;
            return View("SpisakNeRezKarata");
        }
        public ActionResult IndexRezKarte()
        {
            SesijaStart();
            foreach (var item in podaciZaSesiju.Karte.Values)
            {
                if (item.Kupac.KorisnickoIme==podaciZaSesiju.UlogovaniKorisnik.KorisnickoIme && item.DaLiJeObrisana == false && item.StatusKarte == StatusKarte.REZERVISANA)
                {
                    karteZ.Add(item.Id, item);

                }
            }
          
            

            ViewBag.spisak = karteZ;
           
            return View("RezervisaneKarte");
        }
        public ActionResult IndexZaduzenja()
        {
            SesijaStart();
            ViewBag.zaduzenje = podaciZaSesiju.UlogovaniKorisnik.UkupanDug;
            ViewBag.bodovi = podaciZaSesiju.UlogovaniKorisnik.BrojSakupljenihBodova;
            return View("ZaduzenjaIBodovi");
        }
       
        public ActionResult OdobreniKomentari()
        {
            SesijaStart();
            foreach(var item in podaciZaSesiju.Komentari.Values)
            {
                if(item.DaLiJeIzbrisan==false && item.DaLiJePrihvacen==true)
                {
                    odobreniKomentari.Add(item.Id, item);
                }
            }
            ViewBag.komentari = odobreniKomentari;
            return View("KomentariOdobreni");
        }

        public ActionResult RezervisiKartu(int id)//id karte
        {
            SesijaStart();
            foreach(var item in podaciZaSesiju.Karte.Values)
            {
                if(item.Id==id)
                {
                    podaciZaSesiju.Karte[item.Id].StatusKarte = StatusKarte.REZERVISANA;
                    podaciZaSesiju.UlogovaniKorisnik.BrojSakupljenihBodova += item.Cena / 1000 * 133;
                    podaciZaSesiju.UlogovaniKorisnik.UkupanDug += podaciZaSesiju.Karte[item.Id].Cena;
                    podaciZaSesiju.Karte[item.Id].Kupac = podaciZaSesiju.UlogovaniKorisnik;
                    podaciZaSesiju.IzmeniKarte();
                  
                }
            }

            foreach (var item in podaciZaSesiju.Karte.Values)
            {
                if (item.Manifestacija.Id ==podaciZaSesiju.Karte[id].Manifestacija.Id && item.DaLiJeObrisana == false && item.StatusKarte == StatusKarte.NEREZERVISANA)
                {
                    karteN.Add(item.Id, item);

                }
            }
            ViewBag.spisak = karteN;
            return View("SpisakNeRezKarata");

        }
        public ActionResult Odustani(int id)//id karte
        {
            SesijaStart();
            foreach (var item in podaciZaSesiju.Karte.Values)
            {
                if (item.Id == id)
                {
                    if (DateTime.Now <item.Manifestacija.DatumiVremeOdrzavanja.Date.AddDays(7))
                    {
                        podaciZaSesiju.Karte[item.Id].StatusKarte = StatusKarte.NEREZERVISANA;
                        podaciZaSesiju.UlogovaniKorisnik.BrojSakupljenihBodova -= item.Cena / 1000 * 133*4;
                        podaciZaSesiju.UlogovaniKorisnik.UkupanDug -= podaciZaSesiju.Karte[item.Id].Cena;
                        podaciZaSesiju.IzmeniKarte();
                        karteZ.Remove(id);
                        karteN.Add(item.Id, item);
                        ViewBag.dobro = "Uspesno ste odustali!";
                        return View("Uspesno");
                        
                    }
                    
                }
            }
            ViewBag.greska = "Ne mozete odustati jos uvek!";
            return View("GreskaOdustajanja");
        }

        public ActionResult KarteKojimaJeManProsla()
        {
            SesijaStart();
            foreach(var item in podaciZaSesiju.Karte.Values)
            {
                if (item.DaLiJeObrisana == false && item.Manifestacija.DatumiVremeZavrsetka.Date < DateTime.Now.Date && item.StatusKarte == StatusKarte.REZERVISANA)
                {
                   
                        
                        proslaRezervacija.Add(item.Manifestacija.Id, item.Manifestacija);
                        //ViewBag.prosle = proslaRezervacija;
                       
                    
                }
          
            }
            ViewBag.prosle = proslaRezervacija;
            return View("RezProslihMan");
        }

        private void SesijaStart()
        {
            podaciZaSesiju = (OperacijePodataka)Session["PodaciZaSesiju"];
            if (podaciZaSesiju == null)
            {
                podaciZaSesiju = new OperacijePodataka();
                Session["PodaciZaSesiju"] = podaciZaSesiju;
            }

            podaciZaSesiju.UcitajPodatkeIznova();

        }
    }
}