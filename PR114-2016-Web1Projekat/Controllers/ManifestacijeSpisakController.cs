﻿using PR114_2016_Web1Projekat.Baza;
using PR114_2016_Web1Projekat.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PR114_2016_Web1Projekat.Controllers
{
    public class ManifestacijeSpisakController : Controller
    {
        OperacijePodataka podaciZaSesiju;
        // GET: ManifestacijeSpisak
        public ActionResult Index()
        {
            SesijaStart();
            ViewBag.sortiranRecnikManifestacija = NajskorijaManifestacija();
            return View("Manifestacije");
        }

        public Dictionary<string, ManifestacijaModel> NajskorijaManifestacija()
        {
            DateTime danasnjiDatum = DateTime.Now;
            
            Dictionary<string, ManifestacijaModel> noviRecnikManifestacija = new Dictionary<string, ManifestacijaModel>();
            foreach(var item in podaciZaSesiju.Manifestacije.Values)
            {
                if(item.DatumiVremeOdrzavanja.Month==danasnjiDatum.Month)
                {
                    if(item.DatumiVremeOdrzavanja.Day>danasnjiDatum.Day)
                    {
                        noviRecnikManifestacija.Add(item.Naziv, item);
                    }
                    else if(item.DatumiVremeOdrzavanja.Day == danasnjiDatum.Day && item.DatumiVremeOdrzavanja.Hour>danasnjiDatum.Hour)
                    {
                        noviRecnikManifestacija.Add(item.Naziv, item);
                    }
                }
                else if(item.DatumiVremeOdrzavanja.Month>danasnjiDatum.Month)
                {
                    noviRecnikManifestacija.Add(item.Naziv, item);
                }
                else if(item.DatumiVremeOdrzavanja.Year>danasnjiDatum.Year)
                {
                    noviRecnikManifestacija.Add(item.Naziv, item);
                }
 
            }
            return noviRecnikManifestacija.OrderBy(x => x.Value.DatumiVremeOdrzavanja).ToDictionary(x => x.Key, x => x.Value);

        }




        private void SesijaStart()
        {
            podaciZaSesiju = (OperacijePodataka)Session["PodaciZaSesiju"];
            if (podaciZaSesiju == null)
            {
                podaciZaSesiju = new OperacijePodataka();
                Session["PodaciZaSesiju"] = podaciZaSesiju;
            }

            podaciZaSesiju.UcitajPodatkeIznova();

        }
    }
}