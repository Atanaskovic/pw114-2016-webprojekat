﻿using PR114_2016_Web1Projekat.Baza;
using PR114_2016_Web1Projekat.Enums;
using PR114_2016_Web1Projekat.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PR114_2016_Web1Projekat.Controllers
{
    public class KomentarController : Controller
    {
        OperacijePodataka podaciZaSesiju;
        Dictionary<int, ManifestacijaModel> proslaRezervacija = new Dictionary<int, ManifestacijaModel>();
        // GET: Komentar
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult IndexKomentar(int id)
        {
            ViewBag.id = id;
            return View("DodajKomentar");
        }

        public ActionResult DodajKomentar(KomentarModel formKomentar)
        {
            SesijaStart();
            if (formKomentar.TekstKomentara == null || formKomentar.Ocena==0)
            {
                ViewBag.greska = "Niste uneli sva polja! Pokusajte ponovo!";
                return View("DodavanjeKomentarGreska");
            }
            if (podaciZaSesiju.Komentari.Count > 0)
            {
                foreach (var item in podaciZaSesiju.Komentari.Values)
                {
                    
                    if (item.Kupac.KorisnickoIme == podaciZaSesiju.UlogovaniKorisnik.KorisnickoIme && item.Manifestacija.Id==formKomentar.Manifestacija.Id)
                    {
                        ViewBag.greska = "Vec ste dodali komentar na ovu manifestaciju!";
                        return View("DodavanjeKomentarGreska");
                    }
                }
            }

            KomentarModel komentar = new KomentarModel();
            komentar.Ocena = formKomentar.Ocena;
            komentar.TekstKomentara = formKomentar.TekstKomentara;
            komentar.Id = podaciZaSesiju.Komentari.Count + 1;
            komentar.Manifestacija.Id = formKomentar.Manifestacija.Id;
            komentar.Kupac = podaciZaSesiju.UlogovaniKorisnik;
            podaciZaSesiju.Komentari.Add(komentar.Id, komentar);
            podaciZaSesiju.UpisiKomentarUFajl(komentar);
            foreach (var item in podaciZaSesiju.Karte.Values)
            {
                if (item.DaLiJeObrisana == false && item.Manifestacija.DatumiVremeZavrsetka < DateTime.Now && item.StatusKarte == StatusKarte.REZERVISANA)
                {
                    proslaRezervacija.Add(item.Manifestacija.Id, item.Manifestacija);
                    ViewBag.prosle = proslaRezervacija;
                }
            }
            return View("../Kupac/RezProslihMan");
        }

        private void SesijaStart()
        {
            podaciZaSesiju = (OperacijePodataka)Session["PodaciZaSesiju"];
            if (podaciZaSesiju == null)
            {
                podaciZaSesiju = new OperacijePodataka();
                Session["PodaciZaSesiju"] = podaciZaSesiju;
            }

            podaciZaSesiju.UcitajPodatkeIznova();

        }
    }
}