﻿using PR114_2016_Web1Projekat.Baza;
using PR114_2016_Web1Projekat.Enums;
using PR114_2016_Web1Projekat.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PR114_2016_Web1Projekat.Controllers
{
    public class AdminController : Controller
    {
        OperacijePodataka podaciZaSesiju;
        Dictionary<int, ManifestacijaModel> novo = new Dictionary<int, ManifestacijaModel>();
        // GET: Admin
        public ActionResult Index()
        {
            SesijaStart();
            
            return View("DodajKupca");
        }
        public ActionResult Index1()
        {
            SesijaStart();
            ViewBag.Manifestacije = podaciZaSesiju.Manifestacije;
            ViewBag.aktivan = StatusManifestacije.AKTIVNO;
            return View("IzlistajManifestacije");
        }
        public ActionResult IndexKarte()
        {
            SesijaStart();
            ViewBag.karte = podaciZaSesiju.Karte;
            return View("IzlistajKarte");
        }
        public ActionResult IndexKupci()
        {
            SesijaStart();
            ViewBag.kupci = podaciZaSesiju.Kupci;
            return View("IzlistajKorisnike");
        }
        public ActionResult IndexProdavci()
        {
            SesijaStart();
            ViewBag.prodavci = podaciZaSesiju.Prodavci;
            return View("IzlistajProdavce");
        }
        public ActionResult Komentari()
        {
            SesijaStart();
            ViewBag.komentari= podaciZaSesiju.Komentari;
            return View("VidiKomentar");
        }
        [HttpPost]
        public ActionResult DodajProdavca(KorisnikModel formProdavac)
        {
           
            SesijaStart();
            //sva polja moraju biti popunjena, ako ne, prebaci na view greska
            if (formProdavac.KorisnickoIme == null || formProdavac.Ime == null || formProdavac.Prezime == null)
            {
                ViewBag.greska = "Niste uneli sva polja pri registraciji! Pokusajte ponovo!";
                return View("GreskaDodavanje");
            }
            //korisnicko ime pri registraciji mora da se razlikuje od svih u "bazi", ako ne, prebaci na view greska
            foreach (var a in podaciZaSesiju.Kupci.Values)
            {
                if (a.KorisnickoIme == formProdavac.KorisnickoIme)
                {
                    ViewBag.greska = "Korisnik sa ovim korisnickim imenom vec postoji!";
                    return View("GreskaDodavanja");
                }
            }
            foreach (var a in podaciZaSesiju.Administratori.Values)
            {
                if (a.KorisnickoIme == formProdavac.KorisnickoIme)
                {
                    ViewBag.greska = "Korisnik sa ovim korisnickim imenom vec postoji!";
                    return View("GreskaDodavanja");
                }
            }
            foreach (var a in podaciZaSesiju.Prodavci.Values)
            {
                if (a.KorisnickoIme == formProdavac.KorisnickoIme)
                {
                    ViewBag.greska = "Korisnik sa ovim korisnickim imenom vec postoji!";
                    return View("GreskaDodavanja");
                }
            }
            //preuzimanje podataka iz forme
            KorisnikModel prodavac = new KorisnikModel();
            prodavac.KorisnickoIme = formProdavac.KorisnickoIme;
            prodavac.Lozinka = formProdavac.Lozinka;
            prodavac.Ime = formProdavac.Ime;
            prodavac.Prezime = formProdavac.Prezime;
            prodavac.DatumRodjenja = formProdavac.DatumRodjenja;
            prodavac.Pol = formProdavac.Pol;
            prodavac.UlogaKorisnika = Enums.Uloga.PRODAVAC;
            //popunjavanje podataka svih
            podaciZaSesiju.Prodavci.Add(prodavac.KorisnickoIme, prodavac);
            podaciZaSesiju.UpisiProdavcaUFajl(prodavac);
            // podaciZaSesiju.UlogovaniKorisnik = kupac;
            ViewBag.registrovan = podaciZaSesiju.UlogovaniKorisnik;//ovaj korisnik je trenutno ulogovan
            return View("../LogovanKorisnik/LogovanKorisnikAdministrator");
        }
       
        public ActionResult AktivirajManifestaciju(int id)
        {
            SesijaStart();
            foreach(var item in podaciZaSesiju.Manifestacije.Values)
            {
                if(item.Id==id)
                {
                    if(podaciZaSesiju.Manifestacije[item.Id].Status==StatusManifestacije.NEAKTIVNO)
                    {
                        podaciZaSesiju.Manifestacije[item.Id].Status = StatusManifestacije.AKTIVNO;
                        podaciZaSesiju.IzmeniManifestacije();
                    }
                    else
                    {
                        ViewBag.greska = "Ova manifestacija je vec aktivna!";
                        return View("GreskaAktivacije");
                    }
                }
            }
            ViewBag.manifestacije = podaciZaSesiju.Manifestacije;
            return View("IzlistajManifestacije");

        }
        public ActionResult ObrisiManifestaciju(int id)
        {

            SesijaStart();
            foreach(var item in podaciZaSesiju.Manifestacije.Values)
            {
                if(item.Id==id)
                {
                    if(podaciZaSesiju.Manifestacije[item.Id].DaLiJeObrisana!=true)
                    {
                        podaciZaSesiju.Manifestacije[item.Id].DaLiJeObrisana = true;
                        podaciZaSesiju.IzmeniManifestacije();

                        foreach(var i in podaciZaSesiju.Karte.Values)
                        {
                            if(i.Manifestacija.Id==item.Id)
                            {
                                i.DaLiJeObrisana = true;
                                podaciZaSesiju.IzmeniKarte();
                            }
                        }
                        foreach (var i in podaciZaSesiju.Komentari.Values)
                        {
                            if (i.Manifestacija.Id == item.Id)
                            {
                                i.DaLiJeIzbrisan = true;
                                podaciZaSesiju.IzmeniKomentare();
                            }
                        }
                    }
                }

            }
           
            ViewBag.manifestacije = podaciZaSesiju.Manifestacije;
            return View("IzlistajManifestacije");
        }

        public ActionResult ObrisiProdavca(string korisnickoIme)
        {
            SesijaStart();
            foreach (var item in podaciZaSesiju.Prodavci.Values)
            {
                if (item.KorisnickoIme==korisnickoIme)
                {
                    if (podaciZaSesiju.Prodavci[item.KorisnickoIme].DaLiJeObrisana != true)
                    {
                        podaciZaSesiju.Prodavci[item.KorisnickoIme].DaLiJeObrisana = true;
                        podaciZaSesiju.IzmeniProdavce();

                        foreach (var i in podaciZaSesiju.Manifestacije.Values)
                        {
                            if (i.ProdavacKojiJeKreirao.KorisnickoIme == item.KorisnickoIme)
                            {
                                i.DaLiJeObrisana = true;
                                podaciZaSesiju.IzmeniManifestacije();

                                foreach (var j in podaciZaSesiju.Karte.Values)
                                {
                                    if (j.Manifestacija.Id == i.Id && j.DaLiJeObrisana!=true)
                                    {
                                        j.DaLiJeObrisana = true;
                                        podaciZaSesiju.IzmeniKarte();
                                    }
                                }

                            }
                        }

                    }
                }

            }

            ViewBag.prodavci = podaciZaSesiju.Prodavci;
            return View("IzlistajProdavce");

        }
        public ActionResult ObrisiKupca(string korisnickoIme)
        {

            SesijaStart();
            foreach (var item in podaciZaSesiju.Kupci.Values)
            {
                if (item.KorisnickoIme == korisnickoIme)
                {
                    if (podaciZaSesiju.Kupci[item.KorisnickoIme].DaLiJeObrisana != true)
                    {
                        podaciZaSesiju.Kupci[item.KorisnickoIme].DaLiJeObrisana = true;
                        podaciZaSesiju.IzmeniKupce();

                        foreach(var karta in podaciZaSesiju.Karte.Values)
                        {
                            if(karta.Kupac.KorisnickoIme==item.KorisnickoIme)
                            {
                                podaciZaSesiju.Karte[karta.Id].StatusKarte = StatusKarte.NEREZERVISANA;
                                podaciZaSesiju.IzmeniKarte();
                            }


                        }
                        foreach (var komentar in podaciZaSesiju.Komentari.Values)
                        {
                            if (komentar.Kupac.KorisnickoIme == item.KorisnickoIme)
                            {
                                podaciZaSesiju.Komentari[komentar.Id].DaLiJeIzbrisan=true;
                                podaciZaSesiju.IzmeniKomentare();
                            }


                        }
                    }

                }
            }
            ViewBag.kupci = podaciZaSesiju.Kupci;
            return View("IzlistajKorisnike");

        }
        private void SesijaStart()
        {
            podaciZaSesiju = (OperacijePodataka)Session["PodaciZaSesiju"];
            if (podaciZaSesiju == null)
            {
                podaciZaSesiju = new OperacijePodataka();
                Session["PodaciZaSesiju"] = podaciZaSesiju;
            }

            podaciZaSesiju.UcitajPodatkeIznova();

        }
    }
}