﻿using PR114_2016_Web1Projekat.Baza;
using PR114_2016_Web1Projekat.Enums;
using PR114_2016_Web1Projekat.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PR114_2016_Web1Projekat.Controllers
{
    public class ProfilController : Controller
    {
        OperacijePodataka podaciZaSesiju;
        // GET: Profil
        public ActionResult Index()
        {
            SesijaStart();

            ViewBag.uloga = Enums.Uloga.KUPAC;
            ViewBag.ulogovan = podaciZaSesiju.UlogovaniKorisnik;
            ViewBag.date = podaciZaSesiju.UlogovaniKorisnik.DatumRodjenja.Date.ToString("dd/MM/yyyy");
            return View("ProfilKorisnika");
        }
        public ActionResult Index1()
        {
            SesijaStart();
            ViewBag.ulogovan = podaciZaSesiju.UlogovaniKorisnik;
            return View("MenjajProfil");
        }

        [HttpPost]
        public ActionResult IzmeniPodatkeKorisnika(KorisnikModel formIzmeniKorisnika)
        {
            SesijaStart();
            //sva polja moraju biti popunjena, ako ne, prebaci na view greska
            //dodaj za sifru
            if (formIzmeniKorisnika.Ime == null || formIzmeniKorisnika.Prezime == null || formIzmeniKorisnika.KorisnickoIme==null)
            {
                ViewBag.greska = "Niste uneli sva polja pri izmeni podataka! Pokusajte ponovo!";
                return View("GreskaEditovanja");
            }
            //korisnicko ime pri izmeni mora da se razlikuje od svih u "bazi", ako ne, prebaci na view greska
            foreach (var a in podaciZaSesiju.Kupci.Values)
            {
                if (a.KorisnickoIme == formIzmeniKorisnika.KorisnickoIme && a.KorisnickoIme!=podaciZaSesiju.UlogovaniKorisnik.KorisnickoIme)
                {
                    ViewBag.greska = "Korisnik sa ovim korisnickim imenom vec postoji!";
                    return View("GreskaEditovanja");
                }
            }
            foreach (var a in podaciZaSesiju.Administratori.Values)
            {
                if (a.KorisnickoIme == formIzmeniKorisnika.KorisnickoIme && a.KorisnickoIme != podaciZaSesiju.UlogovaniKorisnik.KorisnickoIme)
                {
                    ViewBag.greska = "Korisnik sa ovim korisnickim imenom vec postoji!";
                    return View("GreskaEditovanja");
                }
            }
            foreach (var a in podaciZaSesiju.Prodavci.Values)
            {
                if (a.KorisnickoIme == formIzmeniKorisnika.KorisnickoIme && a.KorisnickoIme != podaciZaSesiju.UlogovaniKorisnik.KorisnickoIme)
                {
                    ViewBag.greska = "Korisnik sa ovim korisnickim imenom vec postoji!";
                    return View("GreskaEditovanja");
                }
            }
            KorisnikModel noviPodaci = new KorisnikModel();
            noviPodaci.KorisnickoIme = formIzmeniKorisnika.KorisnickoIme;
            noviPodaci.Lozinka =formIzmeniKorisnika.Lozinka;
            noviPodaci.Ime = formIzmeniKorisnika.Ime;
            noviPodaci.Prezime = formIzmeniKorisnika.Prezime;
            noviPodaci.DatumRodjenja = podaciZaSesiju.UlogovaniKorisnik.DatumRodjenja;
            noviPodaci.Pol = formIzmeniKorisnika.Pol;
            noviPodaci.UlogaKorisnika = podaciZaSesiju.UlogovaniKorisnik.UlogaKorisnika;
            if(podaciZaSesiju.UlogovaniKorisnik.UlogaKorisnika==Uloga.ADMINISTRATOR)
            {
                podaciZaSesiju.Administratori[podaciZaSesiju.UlogovaniKorisnik.KorisnickoIme] = noviPodaci;
                podaciZaSesiju.IzmeniAdministratore();
            }
            else if(podaciZaSesiju.UlogovaniKorisnik.UlogaKorisnika == Uloga.PRODAVAC)
            {
                podaciZaSesiju.Prodavci[podaciZaSesiju.UlogovaniKorisnik.KorisnickoIme] = noviPodaci;
                //menjamo prodavca i kod manifestacija
                foreach (var item in podaciZaSesiju.Manifestacije)
                {
                    if (item.Value.ProdavacKojiJeKreirao.KorisnickoIme == podaciZaSesiju.UlogovaniKorisnik.KorisnickoIme)
                    {
                        podaciZaSesiju.Manifestacije[item.Key].ProdavacKojiJeKreirao = noviPodaci;
                    }
                }
                podaciZaSesiju.IzmeniManifestacije();
                podaciZaSesiju.IzmeniProdavce();
            }
            else 
            {
                podaciZaSesiju.Kupci[podaciZaSesiju.UlogovaniKorisnik.KorisnickoIme] = noviPodaci;
                //menjamo kupca i kod karata
                foreach (var item in podaciZaSesiju.Karte)
                {
                    if (item.Value.Kupac.KorisnickoIme == podaciZaSesiju.UlogovaniKorisnik.KorisnickoIme)
                    {
                        podaciZaSesiju.Karte[item.Key].Kupac= noviPodaci;
                    }
                }
                //menjamo kupca i kod komentara
                foreach (var item in podaciZaSesiju.Komentari)
                {
                    if (item.Value.Kupac.KorisnickoIme == podaciZaSesiju.UlogovaniKorisnik.KorisnickoIme)
                    {
                        podaciZaSesiju.Komentari[item.Key].Kupac = noviPodaci;
                    }
                }
                podaciZaSesiju.IzmeniKarte();
                podaciZaSesiju.IzmeniKupce();
                podaciZaSesiju.IzmeniKomentare();
                
            }
            podaciZaSesiju.UlogovaniKorisnik = noviPodaci;
            ViewBag.ulogovan = podaciZaSesiju.UlogovaniKorisnik;
            ViewBag.date= podaciZaSesiju.UlogovaniKorisnik.DatumRodjenja.Date.ToString("dd/MM/yyyy");
            return View("ProfilKorisnika");
        }
        private void SesijaStart()
        {
            podaciZaSesiju = (OperacijePodataka)Session["PodaciZaSesiju"];
            if (podaciZaSesiju == null)
            {
                podaciZaSesiju = new OperacijePodataka();
                Session["PodaciZaSesiju"] = podaciZaSesiju;
            }

            podaciZaSesiju.UcitajPodatkeIznova();

        }
    }
}