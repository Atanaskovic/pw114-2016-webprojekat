﻿using PR114_2016_Web1Projekat.Baza;
using PR114_2016_Web1Projekat.Enums;
using PR114_2016_Web1Projekat.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PR114_2016_Web1Projekat.Controllers
{
    public class RegisterController : Controller
    {
        OperacijePodataka podaciZaSesiju = new OperacijePodataka();
        // GET: Register
        public ActionResult Index()
        {

            SesijaStart();
            podaciZaSesiju.UlogovaniKorisnik = null;
            return View("Register");
        }
        [HttpPost]
        public ActionResult RegistrujSe(KorisnikModel formKorisnik)
        {
            SesijaStart();
            //sva polja moraju biti popunjena, ako ne, prebaci na view greska
            //dodaj za sifru
            if(formKorisnik.KorisnickoIme==null  || formKorisnik.Ime==null || formKorisnik.Prezime==null)
            {
                ViewBag.greska = "Niste uneli sva polja pri registraciji! Pokusajte ponovo!";
                return View("GreskaRegistracija");
            }
            //korisnicko ime pri registraciji mora da se razlikuje od svih u "bazi", ako ne, prebaci na view greska
            foreach(var a in podaciZaSesiju.Kupci.Values)
            {
                if(a.KorisnickoIme==formKorisnik.KorisnickoIme && a.DaLiJeObrisana==false)
                {
                    ViewBag.greska = "Korisnik sa ovim korisnickim imenom vec postoji ili je postojao!";
                    return View("GreskaRegistracija");
                }
            }
            foreach (var a in podaciZaSesiju.Administratori.Values)
            {
                if (a.KorisnickoIme == formKorisnik.KorisnickoIme && a.DaLiJeObrisana==false)
                {
                    ViewBag.greska = "Korisnik sa ovim korisnickim imenom vec postoji ili je postojao!";
                    return View("GreskaRegistracija");
                }
            }
            foreach (var a in podaciZaSesiju.Prodavci.Values)
            {
                if (a.KorisnickoIme == formKorisnik.KorisnickoIme && a.DaLiJeObrisana==false)
                {
                    ViewBag.greska = "Korisnik sa ovim korisnickim imenom vec postoji ili je postojao!";
                    return View("GreskaRegistracija");
                }
            }
            //preuzimanje podataka iz forme
            KorisnikModel kupac = new KorisnikModel();
            kupac.KorisnickoIme = formKorisnik.KorisnickoIme;
            kupac.Lozinka = formKorisnik.Lozinka;
            kupac.Ime = formKorisnik.Ime;
            kupac.Prezime = formKorisnik.Prezime;
            kupac.DatumRodjenja = formKorisnik.DatumRodjenja;
            kupac.Pol = formKorisnik.Pol;
            kupac.UlogaKorisnika = Enums.Uloga.KUPAC;
            //popunjavanje podataka svih
            podaciZaSesiju.Kupci.Add(kupac.KorisnickoIme, kupac);
            podaciZaSesiju.UpisiKupcaUFajl(kupac);
           // podaciZaSesiju.UlogovaniKorisnik = kupac;
            //ViewBag.registrovan = podaciZaSesiju.UlogovaniKorisnik;//ovaj korisnik je trenutno ulogovan
            return View("../Entering/Index");

        }
        //korisnik koji vec postoji 
       [HttpPost]
       public ActionResult LogovanjeKorisnika(KorisnikModel formaLogIn)
       {
            SesijaStart();
            if(formaLogIn.KorisnickoIme== null || formaLogIn.Lozinka==null)
            {
                ViewBag.greska = "Morate uneti sva polja pri prijavi! Pokusajte ponovo!";
                return View("GreskaLogin");
            }

            foreach (var a in podaciZaSesiju.Prodavci.Values)
            {
                if (a.KorisnickoIme == formaLogIn.KorisnickoIme)
                {
                   if(a.DaLiJeObrisana==true)
                    {
                        ViewBag.greska = "Zao nam je, ali ovaj korisnik je obrisan iz sistema, od strane admina!";
                        return View("ObrisanGreska");
                    }
                       
                }

            }
            foreach (var a in podaciZaSesiju.Kupci.Values)
            {
                if (a.KorisnickoIme == formaLogIn.KorisnickoIme)
                {
                    if (a.DaLiJeObrisana == true)
                    {
                        ViewBag.greska = "Zao nam je, ali ovaj korisnik je obrisan iz sistema, od strane admina!";
                        return View("ObrisanGreska");
                    }

                }

            }

            //prodji administratore
            foreach (var a in podaciZaSesiju.Administratori.Values)
            {
                if(a.KorisnickoIme==formaLogIn.KorisnickoIme)
                {
                    if(a.Lozinka==formaLogIn.Lozinka)
                    {
                        podaciZaSesiju.UlogovaniKorisnik = a;
                       
                    }
                    else
                    {
                        //pronadjen dobar username, ali je sifra pogresna
                        ViewBag.greska = "Uneli ste pogresnu sifru! Pokusajte ponovo!";
                        return View("GreskaLogin");
                    }

                }
                
            }
            //prodji prodavce
            foreach (var a in podaciZaSesiju.Prodavci.Values)
            {
                if (a.KorisnickoIme == formaLogIn.KorisnickoIme)
                {
                    if (a.Lozinka == formaLogIn.Lozinka)
                    {
                        podaciZaSesiju.UlogovaniKorisnik = a;
                    }
                    else
                    {
                        //pronadjen dobar username, ali je sifra pogresna
                        ViewBag.greska = "Uneli ste pogresnu sifru! Pokusajte ponovo!";
                        return View("GreskaLogin");
                    }

                }

            }
            //prodji kupce
            foreach (var a in podaciZaSesiju.Kupci.Values)
            {
                if (a.KorisnickoIme == formaLogIn.KorisnickoIme)
                {
                    if (a.Lozinka == formaLogIn.Lozinka)
                    {
                        podaciZaSesiju.UlogovaniKorisnik = a;
                    }
                    else
                    {
                        //pronadjen dobar username, ali je sifra pogresna
                        ViewBag.greska = "Uneli ste pogresnu sifru! Pokusajte ponovo!";
                        return View("GreskaLogin");
                    }

                }

            }
            //ako se ulogovao administrator, odvedi ga na njegovu stranicu
            if(podaciZaSesiju.UlogovaniKorisnik.UlogaKorisnika==Uloga.ADMINISTRATOR)
            {
                ViewBag.registrovan = podaciZaSesiju.UlogovaniKorisnik;
                return View("../LogovanKorisnik/LogovanKorisnikAdministrator");
            }
            //ako se ulogovao prodavac, odvedi ga na njegovu stranicu
            else if (podaciZaSesiju.UlogovaniKorisnik.UlogaKorisnika == Uloga.PRODAVAC)
            {
                ViewBag.registrovan = podaciZaSesiju.UlogovaniKorisnik;
                return View("../LogovanKorisnik/LogovanKorisnikProdavac");
            }
            //ako se ulogovao kupac, odvedi ga na njegovu stranicu
            else
            {
                ViewBag.registrovan = podaciZaSesiju.UlogovaniKorisnik;
                return View("../LogovanKorisnik/LogovanKorisnikKupac");
            }   
       }

        //za svakog korisnika se pokrece posebno
        private void SesijaStart()
        {
            podaciZaSesiju = (OperacijePodataka)Session["PodaciZaSesiju"];
            if (podaciZaSesiju == null)
            {
                podaciZaSesiju = new OperacijePodataka();
                Session["PodaciZaSesiju"] = podaciZaSesiju;
            }

            podaciZaSesiju.UcitajPodatkeIznova();

        }
    }
}