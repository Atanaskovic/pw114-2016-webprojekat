﻿using PR114_2016_Web1Projekat.Baza;
using PR114_2016_Web1Projekat.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PR114_2016_Web1Projekat.Controllers
{
    public class LogovanKorisnikController : Controller
    {
        OperacijePodataka podaciZaSesiju;
        // GET: LogovanKorisnik
        public ActionResult Index()
        {
            SesijaStart();
            ViewBag.registrovan = podaciZaSesiju.UlogovaniKorisnik;
            return View("LogovanKorisnikKupac");
        }
        public ActionResult IndexAdmin()
        {
            SesijaStart();
            ViewBag.registrovan = podaciZaSesiju.UlogovaniKorisnik;
            return View("LogovanKorisnikAdministrator");
        }
        public ActionResult IndexProdavac()
        {
            SesijaStart();
            ViewBag.registrovan = podaciZaSesiju.UlogovaniKorisnik;
            return View("LogovanKorisnikProdavac");
        }

        private void SesijaStart()
        {
            podaciZaSesiju = (OperacijePodataka)Session["PodaciZaSesiju"];
            if (podaciZaSesiju == null)
            {
                podaciZaSesiju = new OperacijePodataka();
                Session["PodaciZaSesiju"] = podaciZaSesiju;
            }

            podaciZaSesiju.UcitajPodatkeIznova();

        }
    }
}