﻿using PR114_2016_Web1Projekat.Baza;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PR114_2016_Web1Projekat.Controllers
{
    public class EnteringController : Controller
    {
        OperacijePodataka podaciZaSesiju;
        // GET: Entering
        public ActionResult Index()
        {
           
            SesijaStart();
            return View();
        }

        private void SesijaStart()
        {
            podaciZaSesiju = (OperacijePodataka)Session["PodaciZaSesiju"];
            if (podaciZaSesiju == null)
            {
                podaciZaSesiju = new OperacijePodataka();
                Session["PodaciZaSesiju"] = podaciZaSesiju;
            }

            podaciZaSesiju.UcitajPodatkeIznova();

        }

    }
}