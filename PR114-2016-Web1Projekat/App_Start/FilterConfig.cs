﻿using System.Web;
using System.Web.Mvc;

namespace PR114_2016_Web1Projekat
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
