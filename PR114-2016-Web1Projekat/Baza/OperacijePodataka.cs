﻿using PR114_2016_Web1Projekat.Enums;
using PR114_2016_Web1Projekat.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;

namespace PR114_2016_Web1Projekat.Baza
{
    public class OperacijePodataka
    {
        public Dictionary<string, KorisnikModel> Administratori { get; set; } = new Dictionary<string, KorisnikModel>();//dobavlja se iz fajla
        public Dictionary<string, KorisnikModel> Kupci { get; set; } = new Dictionary<string, KorisnikModel>();
        public Dictionary<string, KorisnikModel> Prodavci { get; set; } = new Dictionary<string, KorisnikModel>();
        public KorisnikModel UlogovaniKorisnik { get; set; } = new KorisnikModel();
        public Dictionary<int, ManifestacijaModel> Manifestacije { get; set; } = new Dictionary<int, ManifestacijaModel>();
        public Dictionary<int, KartaModel> Karte { get; set; } = new Dictionary<int, KartaModel>();
        public Dictionary<int, KomentarModel> Komentari { get; set; } = new Dictionary<int, KomentarModel>();
        //ucitava sve administratore iz fajla, unose se rucno
        public void UcitajAdministratore()
        {
            string path = "~/App_Data/Administratori.txt";
            path = HostingEnvironment.MapPath(path);
            FileStream stream = new FileStream(path, FileMode.Open);
            StreamReader reader = new StreamReader(stream);
            string tekstUFajlu = "";
            //u fajlovima su polja razdvojena zarezom
            while ((tekstUFajlu = reader.ReadLine()) != null)
            {
                string[] polja = tekstUFajlu.Split(',');
                KorisnikModel administrator = new KorisnikModel();
                administrator.KorisnickoIme = polja[0];
                administrator.Lozinka = polja[1];
                administrator.Ime = polja[2];
                administrator.Prezime = polja[3];
                administrator.Pol = (Pol)Enum.Parse(typeof(Pol), polja[4]);
                administrator.UlogaKorisnika = (Uloga)Enum.Parse(typeof(Uloga), polja[5]);
                administrator.DatumRodjenja = DateTime.Parse(polja[6]);
                Administratori.Add(administrator.KorisnickoIme, administrator);
            }
            reader.Close();
            stream.Close();
        }
        //ucitava prodavce, dodate od strane administratora
        public void UcitajProdavce()
        {
            string path = "~/App_Data/Prodavci.txt";
            path = HostingEnvironment.MapPath(path);
            FileStream stream = new FileStream(path, FileMode.Open);
            StreamReader reader = new StreamReader(stream);
            string tekstUFajlu = "";
            //u fajlovima su polja razdvojena zarezom
            while ((tekstUFajlu = reader.ReadLine()) != null)
            {
                string[] polja = tekstUFajlu.Split(',');
                KorisnikModel prodavac = new KorisnikModel();
                prodavac.KorisnickoIme = polja[0];
                prodavac.Lozinka = polja[1];
                prodavac.Ime = polja[2];
                prodavac.Prezime = polja[3];
                prodavac.Pol = (Pol)Enum.Parse(typeof(Pol), polja[4]);
                prodavac.UlogaKorisnika = (Uloga)Enum.Parse(typeof(Uloga), polja[5]);
                prodavac.DatumRodjenja = DateTime.Parse(polja[6]);
                prodavac.DaLiJeObrisana = bool.Parse(polja[7]);
                Prodavci.Add(prodavac.KorisnickoIme, prodavac);
            }
            reader.Close();
            stream.Close();
        }
        //ucitava registrovane kupce
        public void UcitajKupce()
        {
            string path = "~/App_Data/Kupci.txt";
            path = HostingEnvironment.MapPath(path);
            FileStream stream = new FileStream(path, FileMode.Open);
            StreamReader reader = new StreamReader(stream);
            string tekstUFajlu = "";
            //u fajlovima su polja razdvojena zarezom
            while ((tekstUFajlu = reader.ReadLine()) != null)
            {
                string[] polja = tekstUFajlu.Split(',');
                KorisnikModel kupac = new KorisnikModel();
                kupac.KorisnickoIme = polja[0];
                kupac.Lozinka = polja[1];
                kupac.Ime = polja[2];
                kupac.Prezime = polja[3];
                kupac.Pol = (Pol)Enum.Parse(typeof(Pol), polja[4]);
                kupac.UlogaKorisnika = (Uloga)Enum.Parse(typeof(Uloga), polja[5]);
                kupac.DatumRodjenja = DateTime.Parse(polja[6]);
                kupac.DaLiJeObrisana = bool.Parse(polja[7]);
                Kupci.Add(kupac.KorisnickoIme, kupac);
            }
            reader.Close();
            stream.Close();
        }
        //ucitava manifestacije
        public void UcitajManifestacije()
        {
            string path = "~/App_Data/Manifestacije.txt";
            path = HostingEnvironment.MapPath(path);
            FileStream stream = new FileStream(path, FileMode.Open);
            StreamReader reader = new StreamReader(stream);
            string tekstUFajlu = "";
            //u fajlovima su polja razdvojena zarezom
            while ((tekstUFajlu = reader.ReadLine()) != null)
            {
                string[] polja = tekstUFajlu.Split(',');
                ManifestacijaModel manifestacija = new ManifestacijaModel();
                manifestacija.Id = Int32.Parse(polja[0]);
                manifestacija.Naziv = polja[1];
                manifestacija.TipManifestacije = (TipManifestacije)Enum.Parse(typeof(TipManifestacije), polja[2]);
                manifestacija.BrojMesta = Int32.Parse(polja[3]);
                manifestacija.DatumiVremeOdrzavanja = DateTime.Parse(polja[4]);
                manifestacija.DatumiVremeZavrsetka = DateTime.Parse(polja[5]);
                manifestacija.CenaREGULARKarte = Double.Parse(polja[6]);
                manifestacija.Status = (StatusManifestacije)Enum.Parse(typeof(StatusManifestacije), polja[7]);
                manifestacija.MestoOdrzavanja.UlicaiBrojGradPostanskiBroj = polja[8];
                manifestacija.PosterURL = polja[9];
                manifestacija.ProdavacKojiJeKreirao.KorisnickoIme = polja[10];
                manifestacija.DaLiJeObrisana = bool.Parse(polja[11]);
                Manifestacije.Add(manifestacija.Id, manifestacija);
            }
            reader.Close();
            stream.Close();

        }
        //ucitaj karte
        public void UcitajKarte()
        {
            string path = "~/App_Data/Karte.txt";
            path = HostingEnvironment.MapPath(path);
            FileStream stream = new FileStream(path, FileMode.Open);
            StreamReader reader = new StreamReader(stream);
            string tekstUFajlu = "";
            //u fajlovima su polja razdvojena zarezom
            while ((tekstUFajlu = reader.ReadLine()) != null)
            {
                string[] polja = tekstUFajlu.Split(',');
                KartaModel karta = new KartaModel(); 
                karta.Id = Int32.Parse(polja[0]);
                karta.Manifestacija.Id = Int32.Parse(polja[1]);
                karta.Manifestacija.Naziv = polja[2];
                karta.Manifestacija.DatumiVremeOdrzavanja = DateTime.Parse(polja[3]);
                karta.Manifestacija.DatumiVremeZavrsetka = DateTime.Parse(polja[4]);
                karta.Cena = Int32.Parse(polja[5]);
                karta.Kupac.KorisnickoIme = polja[6];
                karta.StatusKarte = (StatusKarte)Enum.Parse(typeof(StatusKarte), polja[7]);
                karta.TipKarte= (TipKarte)Enum.Parse(typeof(TipKarte), polja[8]);
                karta.DaLiJeObrisana = bool.Parse(polja[9]);
                Karte.Add(karta.Id, karta);
            }
            reader.Close();
            stream.Close();

        }
        //ucitaj komentare
        public void UcitajKomentare()
        {
            string path = "~/App_Data/Komentari.txt";
            path = HostingEnvironment.MapPath(path);
            FileStream stream = new FileStream(path, FileMode.Open);
            StreamReader reader = new StreamReader(stream);
            string tekstUFajlu = "";
            //u fajlovima su polja razdvojena zarezom
            while ((tekstUFajlu = reader.ReadLine()) != null)
            {
                string[] polja = tekstUFajlu.Split(',');
                KomentarModel komentar = new KomentarModel();
                komentar.Id = Int32.Parse(polja[0]);
                komentar.Kupac.KorisnickoIme = polja[1];
                komentar.Manifestacija.Id = Int32.Parse(polja[2]);
                komentar.TekstKomentara = polja[3];
                komentar.Ocena = Int32.Parse(polja[4]);
                komentar.DaLiJePrihvacen = bool.Parse(polja[5]);
                komentar.DaLiJeIzbrisan = bool.Parse(polja[6]);
                Komentari.Add(komentar.Id, komentar);
            }
            reader.Close();
            stream.Close();

        }
        //upisi registrovanog korisnika u fajl Kupci.txt
        public void UpisiKupcaUFajl(KorisnikModel korisnik)
        {
            string path = "~/App_Data/Kupci.txt";
            path = HostingEnvironment.MapPath(path);
            FileStream stream = new FileStream(path, FileMode.Append);//izmeni fajl koji postoji
            StreamWriter writer = new StreamWriter(stream);

            writer.WriteLine(korisnik.ToString());

            writer.Close();
            stream.Close();
        }
        //upisi dodatog prodavca u fajl Prodavci.txt
        public void UpisiProdavcaUFajl(KorisnikModel korisnik)
        {
            string path = "~/App_Data/Prodavci.txt";
            path = HostingEnvironment.MapPath(path);
            FileStream stream = new FileStream(path, FileMode.Append);//izmeni fajl koji postoji
            StreamWriter writer = new StreamWriter(stream);

            writer.WriteLine(korisnik.ToString());

            writer.Close();
            stream.Close();
        }
        public void UpisiManifestacijeUFajl(ManifestacijaModel manifestacija)
        {
            string path = "~/App_Data/Manifestacije.txt";
            path = HostingEnvironment.MapPath(path);
            FileStream stream = new FileStream(path, FileMode.Append);//izmeni fajl koji postoji
            StreamWriter writer = new StreamWriter(stream);

            writer.WriteLine(manifestacija.ToString());

            writer.Close();
            stream.Close();
        }
        //upisi karte u fajl
        public void UpisiKarteUFajl (KartaModel karta)
        {
            string path = "~/App_Data/Karte.txt";
            path = HostingEnvironment.MapPath(path);
            FileStream stream = new FileStream(path, FileMode.Append);//izmeni fajl koji postoji
            StreamWriter writer = new StreamWriter(stream);

            writer.WriteLine(karta.ToString());

            writer.Close();
            stream.Close();
        }
        //upisi komentare u fajl
        public void UpisiKomentarUFajl(KomentarModel komentar)
        {
            string path = "~/App_Data/Komentari.txt";
            path = HostingEnvironment.MapPath(path);
            FileStream stream = new FileStream(path, FileMode.Append);//izmeni fajl koji postoji
            StreamWriter writer = new StreamWriter(stream);

            writer.WriteLine(komentar.ToString());

            writer.Close();
            stream.Close();
        }
        //kada menjamo podatke administratoru
        public void IzmeniAdministratore()
        {
            string path = "~/App_Data/Administratori.txt";
            path = HostingEnvironment.MapPath(path);
            FileStream stream = new FileStream(path, FileMode.Create);
            StreamWriter writer = new StreamWriter(stream);

            foreach (var item in Administratori.Values)
            {
                writer.WriteLine(item.ToString());
            }

            writer.Close();
            stream.Close();
        }
        //kad menjamo podatke prodavaca
        public void IzmeniProdavce()
        {
            string path = "~/App_Data/Prodavci.txt";
            path = HostingEnvironment.MapPath(path);
            FileStream stream = new FileStream(path, FileMode.Create);
            StreamWriter writer = new StreamWriter(stream);

            foreach (var item in Prodavci.Values)
            {
                writer.WriteLine(item.ToString());
            }

            writer.Close();
            stream.Close();

        }
        //za menjanje manifestacija
        public void IzmeniManifestacije()
        {
            string path = "~/App_Data/Manifestacije.txt";
            path = HostingEnvironment.MapPath(path);
            FileStream stream = new FileStream(path, FileMode.Create);
            StreamWriter writer = new StreamWriter(stream);

            foreach (var item in Manifestacije.Values)
            {
                writer.WriteLine(item.ToString());
            }

            writer.Close();
            stream.Close();
        }
        //kada menjamo podatke kupcima
        public void IzmeniKupce()
        {
            string path = "~/App_Data/Kupci.txt";
            path = HostingEnvironment.MapPath(path);
            FileStream stream = new FileStream(path, FileMode.Create);
            StreamWriter writer = new StreamWriter(stream);

            foreach (var item in Kupci.Values)
            {
                writer.WriteLine(item.ToString());
            }

            writer.Close();
            stream.Close();
        }
        //izmeni karte
        public void IzmeniKarte()
        {
            string path = "~/App_Data/Karte.txt";
            path = HostingEnvironment.MapPath(path);
            FileStream stream = new FileStream(path, FileMode.Create);
            StreamWriter writer = new StreamWriter(stream);

            foreach (var item in Karte.Values)
            {
                writer.WriteLine(item.ToString());
            }

            writer.Close();
            stream.Close();
        }
        //izmeni komentare
        public void IzmeniKomentare()
        {
            string path = "~/App_Data/Komentari.txt";
            path = HostingEnvironment.MapPath(path);
            FileStream stream = new FileStream(path, FileMode.Create);
            StreamWriter writer = new StreamWriter(stream);

            foreach (var item in Komentari.Values)
            {
                writer.WriteLine(item.ToString());
            }

            writer.Close();
            stream.Close();
        }
        //podaci se iznova ucitavaju onda kad sesija zapocne za drugog ulogovanog korisnika
        public void UcitajPodatkeIznova()
        {
            Administratori.Clear();
            Prodavci.Clear();
            Kupci.Clear();
            Manifestacije.Clear();
            Karte.Clear();
            Komentari.Clear();

            UcitajKupce();
            UcitajAdministratore();
            UcitajProdavce();
            UcitajManifestacije();
            UcitajKarte();
            UcitajKomentare();
        }




    }
}