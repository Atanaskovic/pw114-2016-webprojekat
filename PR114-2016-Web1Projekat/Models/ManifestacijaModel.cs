﻿using PR114_2016_Web1Projekat.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PR114_2016_Web1Projekat.Models
{        
    public class ManifestacijaModel
    {
       
        public int Id { get; set; }
        public string Naziv { get; set; }
        public TipManifestacije TipManifestacije { get; set; }
        public int BrojMesta { get; set; }
        public DateTime DatumiVremeOdrzavanja { get; set; }
        public DateTime DatumiVremeZavrsetka { get; set; }
        public double CenaREGULARKarte { get; set; }
        public StatusManifestacije Status { get; set; }
        public MestoOdrzavanjaModel MestoOdrzavanja { get; set; } = new MestoOdrzavanjaModel();
        public string PosterURL { get; set; }
        public KorisnikModel ProdavacKojiJeKreirao { get; set; } = new KorisnikModel();
        public bool DaLiJeObrisana { get; set; } = false;

        public ManifestacijaModel()
        {
            
        }

        public override string ToString()
        {
            return this.Id + "," +
                this.Naziv + "," +
                this.TipManifestacije.ToString() + "," +
                this.BrojMesta + "," +
                this.DatumiVremeOdrzavanja + "," +
                this.DatumiVremeZavrsetka + "," +
                this.CenaREGULARKarte + "," +
                this.Status.ToString() + "," +
                this.MestoOdrzavanja.UlicaiBrojGradPostanskiBroj + "," +
                this.PosterURL + "," +
                this.ProdavacKojiJeKreirao.KorisnickoIme + "," +
                this.DaLiJeObrisana.ToString();
        }

    }
}