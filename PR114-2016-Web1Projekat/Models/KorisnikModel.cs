﻿using PR114_2016_Web1Projekat.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PR114_2016_Web1Projekat.Models
{
    public class KorisnikModel
    {
        public string KorisnickoIme { get; set; }
        public string Lozinka { get; set; }
        public string Ime { get; set; }
        public string Prezime { get; set; }
        public Pol Pol { get; set; }
        public DateTime DatumRodjenja { get; set; }
        public Uloga UlogaKorisnika { get; set; }
        public List<KartaModel> SveKarteBezObziraNaStatus { get; set; } = new List<KartaModel>();//ako je kupac
        public List<ManifestacijaModel> Manifestacije { get; set; } = new List<ManifestacijaModel>();//ako je prodavac
        public double BrojSakupljenihBodova { get; set; }//ako je kupac
        public TipKorisnikaModel TipKorisnika { get; set; } = new TipKorisnikaModel();
        public bool DaLiJeObrisana { get; set; } = false;
        public int BrojRezervisanihKarata { get; set; }
        public double UkupanDug { get; set; }
        

        public KorisnikModel()
        {
            this.BrojSakupljenihBodova = 0;
        }
        //za ispisivanje u fajl
        public override string ToString()
        {
            return this.KorisnickoIme + "," + this.Lozinka + "," + this.Ime + "," + this.Prezime + "," + this.Pol.ToString() + "," + this.UlogaKorisnika.ToString()+ "," +this.DatumRodjenja + "," + this.DaLiJeObrisana.ToString();
        }

    }
}