﻿using PR114_2016_Web1Projekat.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PR114_2016_Web1Projekat.Models
{
    public class TipKorisnikaModel
    {
        public ImeTipa ImeTipaKorisnika { get; set;}
        public double Popust { get; set; }
        public int TrazeniBrojBodovaZlatni { get; set; } 
        public int TrazeniBrojBodovaSrebrni { get; set; } 
        public int TrazeniBrojBodovaBronzani { get; set; } 

        public TipKorisnikaModel()
        {
            this.TrazeniBrojBodovaBronzani = 30;
            this.TrazeniBrojBodovaSrebrni = 50;
            this.TrazeniBrojBodovaZlatni = 100;
        }
    }
}