﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PR114_2016_Web1Projekat.Models
{
    public class KomentarModel
    {
        public int Id { get; set; }
        public KorisnikModel Kupac { get; set; } = new KorisnikModel();
        public ManifestacijaModel Manifestacija { get; set; } = new ManifestacijaModel();
        public string TekstKomentara { get; set; }
        public int Ocena { get; set; }//od 1 do 5
        public bool DaLiJePrihvacen { get; set; } = false;
        public bool DaLiJeIzbrisan { get; set; } = false;
        public KomentarModel()
        {
        }
        public override string ToString()
        {
            return this.Id + "," +
                   this.Kupac.KorisnickoIme + "," +
                   this.Manifestacija.Id + "," +
                   this.TekstKomentara + "," +
                   this.Ocena + "," +
                   this.DaLiJePrihvacen + "," +
                   this.DaLiJeIzbrisan;
        }
    }
}