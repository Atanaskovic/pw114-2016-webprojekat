﻿using PR114_2016_Web1Projekat.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PR114_2016_Web1Projekat.Models
{
    public class KartaModel
    {
        public int Id { get; set; }
        public ManifestacijaModel Manifestacija { get; set; } = new ManifestacijaModel();
        public double Cena { get; set; }
        public KorisnikModel Kupac { get; set; } = new KorisnikModel();
        public StatusKarte StatusKarte { get; set; }
        public TipKarte TipKarte { get; set; }
        public bool DaLiJeObrisana { get; set; } = false;

        public KartaModel()
        {
           

        }

        public override string ToString()
        {
            return this.Id + "," +
                   this.Manifestacija.Id + "," +
                   this.Manifestacija.Naziv + "," +
                   this.Manifestacija.DatumiVremeOdrzavanja + "," +
                   this.Manifestacija.DatumiVremeZavrsetka + "," +
                   this.Cena + "," +
                   this.Kupac.KorisnickoIme + "," +
                   this.StatusKarte.ToString() + "," +
                   this.TipKarte.ToString() + "," +
                   this.DaLiJeObrisana.ToString();

        }




    }
}